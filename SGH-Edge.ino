#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPClient.h>

/*Wifi Credential*/
const char * ssid = "Camera-AP";
const char * password = "Raffaele94!";

/*Analog Variables*/
const int analogInPin = A0; // ESP8266 Analog Pin ADC0 = A0
int sensorValue = 0; // value read from the pot
int outputValue = 0; // value to output to a PWM pin
unsigned long last_sent_time = millis(); // time last counter sent to the receiver host

int accepted_statuscode[2] = {
  200
};

String Link = "https://e598299c.ngrok.io"; // destination

void setup() {

  /*Serial init*/
  delay(1000);
  Serial.begin(9600);

  /*Connection init block*/
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }
}

void loop() {

  /*Init Analog Read*/
  int vint = outputValue;
  String path = "/set_humidity/";

  HTTPClient http;

  unsigned long current_time = millis();

  // send data every second
  if (current_time - last_sent_time > 100) {
    sensorValue = analogRead(analogInPin);
    outputValue = map(sensorValue, 9, 1024, 0, 100); // map it to the range of the PWM out
    http.begin(Link + path + vint);
    int httpCode = http.GET();
    Serial.println(httpCode);
    http.end();
    last_sent_time = millis();
  }
  if (current_time < last_sent_time) {
    // millis reset
    current_time = last_sent_time;
  }

}
